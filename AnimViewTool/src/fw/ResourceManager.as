package fw
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	
	import fw.utils.loading.BigLoader;
	
	public class ResourceManager
	{
		private static var _inst:ResourceManager;
		private var _assetList:Array;
		private var _bulkLoader:BigLoader;
		
		public function ResourceManager()
		{
			_inst = this;
			_assetList= [];
			_bulkLoader = new BigLoader();
		}
		
		// Singleton accessor
		public static function get instance():ResourceManager { return _inst; }
		
		private static const ASSET_DIR:String = "data/";
		
		private var _cbComplete:Function;
		private var _cbFail:Function;
		private var failed:Boolean;
		
		public function loadAssets(arList:Array, responder:Function, fail:Function):void
		{
			_cbComplete= responder;
			_cbFail= fail;
			failed= false;
			
			_bulkLoader.reset();
			var id:String;
			
			for (var i:int=0; i < arList.length; i++) {
				_bulkLoader.add(/*ASSET_DIR +*/ String(arList[i]), stripFileName(arList[i]));
			}
			
			_bulkLoader.addEventListener(Event.COMPLETE, onComplete, false, 0, true);
			_bulkLoader.addEventListener(IOErrorEvent.IO_ERROR, onLoadError, false, 0, true);

			_bulkLoader.start();
		}
		
		protected function onLoadError(event:IOErrorEvent):void
		{
			if (failed) return;
			trace("load assets failed!");
			
			_cbFail(event.text);
			failed= true;
			
			//_bulkLoader.removeEventListener(IOErrorEvent.IO_ERROR, onLoadError);
			//event.stopImmediatePropagation();
		}
		
		public function getAsset(nameId:String):*
		{
			if (_assetList[nameId] == null) {
				trace("getAsset: cannot find asset " + nameId);
			}
			return _assetList[nameId];
		}
		
		public function getBitmapData(nameId:String):BitmapData
		{
			if (_assetList[nameId] == null) {
				trace("getAsset: cannot find asset " + nameId);
				return null;
			}
			
			var srcBmp:Bitmap = Bitmap(getAsset(nameId));
			return srcBmp.bitmapData;
		}
		
		public function getBitmapAsset(nameId:String, w:Number=0):Bitmap
		{
			//var srcBmp:Bitmap = Bitmap(getAsset(nameId));
			var newBmp:Bitmap = new Bitmap(getBitmapData(nameId), "auto", true);
			
			newBmp.smoothing=true;
			if (w > 0) {
				newBmp.width=w;
				newBmp.height=w;
			}
			return newBmp;
		}
		
		private function onComplete(e:Event):void
		{
			_bulkLoader.collectAllItems(_assetList);
			_cbComplete();
		}
		
		private function stripFileName(path:String):String
		{
			var ext:String =  path.substring(path.lastIndexOf(".")+1);
			var file:String = path.substring(path.lastIndexOf("/")+1, path.lastIndexOf("."));
			
			// Hack for duplicate names with different extensions
			if (ext == "json") {
				file+= "_d";
			}
			return file;
		}
		
		
	}
}